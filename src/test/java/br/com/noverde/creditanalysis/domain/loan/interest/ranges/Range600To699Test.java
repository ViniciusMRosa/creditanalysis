package br.com.noverde.creditanalysis.domain.loan.interest.ranges;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class Range600To699Test {

    private Range600To699 range =  new Range600To699();

    @ParameterizedTest
    @MethodSource("validValuesListProvider")
    void shouldReturnValueWhenIsValidTermAndScoreIsBetween600And699(Integer terms,Integer score, Double expected) {
        assertThat(range.calculate(terms,score).get()).isEqualTo(expected);
    }

    private static Stream<Arguments> validValuesListProvider(){
        return Stream.of(
                Arguments.of(6,600,6.4),
                Arguments.of(9,600,6.6),
                Arguments.of(12,600,6.9),
                Arguments.of(6,650,6.4),
                Arguments.of(9,650,6.6),
                Arguments.of(12,650,6.9),
                Arguments.of(6,699,6.4),
                Arguments.of(9,699,6.6),
                Arguments.of(12,699,6.9)
        );
    }



    @ParameterizedTest
    @MethodSource("invalidValuesListProvider")
    void shouldNotReturnValueWhenIsInvalidTermOrScoreIsNotBetween600And699(Integer terms,Integer score) {
        assertThat(range.calculate(terms,score)).isNotPresent();
    }

    private static Stream<Arguments> invalidValuesListProvider(){
        return Stream.of(
                Arguments.of(6,599),
                Arguments.of(9,599),
                Arguments.of(12,599),
                Arguments.of(6,700),
                Arguments.of(9,700),
                Arguments.of(12,700),
                Arguments.of(3,699),
                Arguments.of(3,699),
                Arguments.of(3,699)
        );
    }
}