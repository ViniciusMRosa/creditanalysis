package br.com.noverde.creditanalysis.domain.loan.policies;

import br.com.noverde.creditanalysis.domain.loan.Loan;
import br.com.noverde.creditanalysis.domain.loan.LoanStatus;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

class AgePolicyTest {

    private AgePolicy policy = new AgePolicy();

    @Test
    public void shouldRefuseLoanWhenAgeIsUnder18(){
        var loan = Loan.builder()
                .birthDate(LocalDate.now().minusYears(17))
                .build();

        final Loan result = policy.validate(loan);

        assertThat(result.getStatus()).isEqualByComparingTo(LoanStatus.REFUSED);
    }

    @Test
    public void shouldMaintainUnderAnalysisLoanWhenAgeIs18(){
        var loan = Loan.builder()
                .birthDate(LocalDate.now().minusYears(18))
                .build();

        final Loan result = policy.validate(loan);

        assertThat(result.getStatus()).isEqualByComparingTo(LoanStatus.UNDER_ANALYSIS);
    }

    @Test
    public void shouldMaintainUnderAnalysisLoanWhenAgeIsOver18(){
        var loan = Loan.builder()
                .birthDate(LocalDate.now().minusYears(19))
                .build();

        final Loan result = policy.validate(loan);

        assertThat(result.getStatus()).isEqualByComparingTo(LoanStatus.UNDER_ANALYSIS);
    }
}