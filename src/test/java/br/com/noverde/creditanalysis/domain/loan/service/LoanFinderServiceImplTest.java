package br.com.noverde.creditanalysis.domain.loan.service;

import br.com.noverde.creditanalysis.application.loan.LoanStatusResponse;
import br.com.noverde.creditanalysis.domain.loan.Loan;
import br.com.noverde.creditanalysis.domain.loan.LoanStatus;
import br.com.noverde.creditanalysis.domain.loan.repository.LoanRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LoanFinderServiceImplTest {

    @InjectMocks
    private LoanFinderServiceImpl service;
    @Mock
    private LoanRepository repository;

    @Test
    public void shouldGetLoanStatusWhenApproved(){
        var loan = Loan.builder()
                .id(UUID.randomUUID())
                .status(LoanStatus.APPROVED)
                .approvedTerms(9)
                .terms(6)
                .amount(1000d)
                .income(10000d)
                .build();
        when(repository.getById(loan.getId()))
                .thenReturn(Optional.of(loan));

        final LoanStatusResponse loanById = service.getLoanById(loan.getId());

        assertThat(loanById.getId()).isEqualTo(loan.getId());
        assertThat(loanById.getStatus()).isEqualTo("completed");
        assertThat(loanById.getResult()).isEqualTo("approved");
        assertThat(loanById.getRefusedPolicy()).isNull();
        assertThat(loanById.getAmount()).isEqualTo(loan.getAmount());
        assertThat(loanById.getTerms()).isEqualTo(loan.getApprovedTerms());
    }

    @Test
    public void shouldGetLoanStatusWhenProcessing(){
        var loan = Loan.builder()
                .id(UUID.randomUUID())
                .status(LoanStatus.UNDER_ANALYSIS)
                .terms(6)
                .amount(1000d)
                .income(10000d)
                .build();
        when(repository.getById(loan.getId()))
                .thenReturn(Optional.of(loan));

        final LoanStatusResponse loanById = service.getLoanById(loan.getId());

        assertThat(loanById.getId()).isEqualTo(loan.getId());
        assertThat(loanById.getStatus()).isEqualTo("processing");
        assertThat(loanById.getResult()).isNull();
        assertThat(loanById.getRefusedPolicy()).isNull();
        assertThat(loanById.getAmount()).isNull();
        assertThat(loanById.getTerms()).isEqualTo(loan.getApprovedTerms());
    }

    @Test
    public void shouldGetLoanStatusWhenRefused(){
        var loan = Loan.builder()
                .id(UUID.randomUUID())
                .status(LoanStatus.REFUSED)
                .terms(6)
                .refusedPolicy("age")
                .amount(1000d)
                .income(10000d)
                .build();
        when(repository.getById(loan.getId()))
                .thenReturn(Optional.of(loan));

        final LoanStatusResponse loanById = service.getLoanById(loan.getId());

        assertThat(loanById.getId()).isEqualTo(loan.getId());
        assertThat(loanById.getStatus()).isEqualTo("completed");
        assertThat(loanById.getResult()).isEqualTo("refused");
        assertThat(loanById.getRefusedPolicy()).isEqualTo(loan.getRefusedPolicy());
        assertThat(loanById.getAmount()).isNull();
        assertThat(loanById.getTerms()).isNull();
    }
}