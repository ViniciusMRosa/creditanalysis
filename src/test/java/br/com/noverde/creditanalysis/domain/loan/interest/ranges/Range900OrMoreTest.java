package br.com.noverde.creditanalysis.domain.loan.interest.ranges;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class Range900OrMoreTest {
    private Range900OrMore range =  new Range900OrMore();

    @ParameterizedTest
    @MethodSource("validValuesListProvider")
    void shouldReturnValueWhenIsValidTermAndScoreIs900OrMore(Integer terms,Integer score, Double expected) {
        assertThat(range.calculate(terms,score).get()).isEqualTo(expected);
    }

    private static Stream<Arguments> validValuesListProvider(){
        return Stream.of(
                Arguments.of(6,900,3.9),
                Arguments.of(9,900,4.2),
                Arguments.of(12,900,4.5),
                Arguments.of(6,950,3.9),
                Arguments.of(9,950,4.2),
                Arguments.of(12,950,4.5),
                Arguments.of(6,999,3.9),
                Arguments.of(9,999,4.2),
                Arguments.of(12,999,4.5)
        );
    }



    @ParameterizedTest
    @MethodSource("invalidValuesListProvider")
    void shouldNotReturnValueWhenIsInvalidTermOrScoreIsNot900OrMore(Integer terms,Integer score) {
        assertThat(range.calculate(terms,score)).isNotPresent();
    }

    private static Stream<Arguments> invalidValuesListProvider(){
        return Stream.of(
                Arguments.of(6,699),
                Arguments.of(9,699),
                Arguments.of(12,699),
                Arguments.of(6,700),
                Arguments.of(9,700),
                Arguments.of(12,700),
                Arguments.of(3,799),
                Arguments.of(3,799),
                Arguments.of(3,799)
        );
    }
}