package br.com.noverde.creditanalysis.domain.loan.service;

import br.com.noverde.creditanalysis.domain.loan.interest.InterestCalculatorChain;
import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PMTCalculatorServiceTest {

    @InjectMocks
    private PMTCalculatorService service;

    @Mock
    private InterestCalculatorChain interestCalculatorChain;


    @Test
    public void shouldCalculatePMT(){
        var terms = 6;
        var score = 900;
        var amount = Double.valueOf(4000);
        var interest = (3.9/100);
        when(interestCalculatorChain.calculateInterest(terms,score))
                .thenReturn(interest);
        final Double pmt = service.calculatePMT(terms, score, amount);

        assertThat(pmt).isEqualTo(760.56, Offset.offset(0.1));
    }
}