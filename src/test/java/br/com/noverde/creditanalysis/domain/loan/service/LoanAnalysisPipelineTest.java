package br.com.noverde.creditanalysis.domain.loan.service;

import br.com.noverde.creditanalysis.domain.loan.Loan;
import br.com.noverde.creditanalysis.domain.loan.LoanStatus;
import br.com.noverde.creditanalysis.domain.loan.policies.AgePolicy;
import br.com.noverde.creditanalysis.domain.loan.policies.CommitmentPolicy;
import br.com.noverde.creditanalysis.domain.loan.policies.ScorePolicy;
import br.com.noverde.creditanalysis.infrastructure.adapters.CommitmentAdapter;
import br.com.noverde.creditanalysis.infrastructure.adapters.CommitmentAdapterResponse;
import br.com.noverde.creditanalysis.infrastructure.adapters.ScoreAdapter;
import br.com.noverde.creditanalysis.infrastructure.adapters.response.ScoreAdapterResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.core.env.Environment;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LoanAnalysisPipelineTest {

    private LoanAnalysisPipeline pipeline;
    private AgePolicy agePolicy;
    private ScorePolicy scorePolicy;
    private CommitmentPolicy commitmentPolicy;
    @Mock
    private CircuitBreakerFactory circuitBreakerFactory;
    @Mock
    private CircuitBreaker circuitBreaker;
    @Mock
    private CircuitBreaker commitmentCircuitBreaker;
    @Mock
    private ScoreAdapter scoreAdapter;
    @Mock
    private CommitmentAdapter commitmentAdapter;
    @Mock
    private Environment environment;
    @Mock
    private PMTCalculatorService pmtCalculatorService;

    @BeforeEach
    public void setup(){
        agePolicy =  new AgePolicy();
        scorePolicy = new ScorePolicy(circuitBreakerFactory,scoreAdapter);
        commitmentPolicy =  new CommitmentPolicy(commitmentAdapter,circuitBreakerFactory,environment,pmtCalculatorService);
        pipeline = new LoanAnalysisPipeline(agePolicy,scorePolicy,commitmentPolicy);
    }

    @Test
    public void shouldAcceptLoan(){
        var loan = Loan.builder()
                .amount(1000d)
                .terms(6)
                .income(10000d)
                .birthDate(LocalDate.now().minusYears(20))
                .build();
        var expectedPMT = 300d;
        when(circuitBreakerFactory.create("score"))
                .thenReturn(circuitBreaker);
        var scoreResponse = new ScoreAdapterResponse();
        scoreResponse.setScore(900);
        when(circuitBreaker.run(any()))
                .thenReturn(scoreResponse);
        when(pmtCalculatorService.calculatePMT(loan.getTerms(),900,loan.getAmount()))
        .thenReturn(expectedPMT);
        when(circuitBreakerFactory.create("commitment"))
                .thenReturn(commitmentCircuitBreaker);
        var commitmentAdapterResponse = new CommitmentAdapterResponse();
        commitmentAdapterResponse.setCommitment(0.3);
        when(commitmentCircuitBreaker.run(any()))
                .thenReturn(commitmentAdapterResponse);

        pipeline.initPipeline();
        final Loan validatedLoan = pipeline.applyCreditAnalysis(loan);

        assertThat(validatedLoan.getStatus()).isEqualTo(LoanStatus.APPROVED);
        assertThat(validatedLoan.getTerms()).isEqualTo(loan.getTerms());
        assertThat(validatedLoan.getPmt()).isEqualTo(expectedPMT);
    }

}