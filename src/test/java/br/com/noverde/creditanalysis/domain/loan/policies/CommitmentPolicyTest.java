package br.com.noverde.creditanalysis.domain.loan.policies;

import br.com.noverde.creditanalysis.domain.loan.Loan;
import br.com.noverde.creditanalysis.domain.loan.LoanStatus;
import br.com.noverde.creditanalysis.domain.loan.service.PMTCalculatorService;
import br.com.noverde.creditanalysis.infrastructure.adapters.CommitmentAdapterResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CommitmentPolicyTest {

    @InjectMocks
    private CommitmentPolicy policy;
    @Mock
    private PMTCalculatorService pmtCalculatorService;
    @Mock
    private CircuitBreakerFactory circuitBreakerFactory;
    @Mock
    private CircuitBreaker circuitBreaker;

    @Test
    public void shouldRefuseIfCommitmentIsGreaterThanPmt(){
        var loan = Loan.builder()
                .amount(2500d)
                .income(1500d)
                .score(900)
                .terms(6)
                .status(LoanStatus.UNDER_ANALYSIS)
                .build();
        var response = new CommitmentAdapterResponse();
        response.setCommitment(0.9);
        when(circuitBreakerFactory.create("commitment"))
            .thenReturn(circuitBreaker);
        when(circuitBreaker.run(any()))
                .thenReturn(response);
        when(pmtCalculatorService.calculatePMT(any(),eq(loan.getScore()),eq(loan.getAmount())))
                .thenReturn(760.56)
                .thenReturn(500d)
                .thenReturn(300d);

        final Loan result = policy.validate(loan);

        assertThat(result.getStatus()).isEqualTo(LoanStatus.REFUSED);
    }

    @Test
    public void shouldAcceptIfCommitmentIsLessThanPmtWith12Terms(){
        var loan = Loan.builder()
                .amount(2500d)
                .income(1900d)
                .score(900)
                .terms(6)
                .status(LoanStatus.UNDER_ANALYSIS)
                .build();
        var response = new CommitmentAdapterResponse();
        response.setCommitment(0.8);
        when(circuitBreakerFactory.create("commitment"))
                .thenReturn(circuitBreaker);
        when(circuitBreaker.run(any()))
                .thenReturn(response);
        when(pmtCalculatorService.calculatePMT(any(),eq(loan.getScore()),eq(loan.getAmount())))
                .thenReturn(760.56)
                .thenReturn(500d)
                .thenReturn(300d);

        final Loan result = policy.validate(loan);

        assertThat(result.getStatus()).isEqualTo(LoanStatus.APPROVED);
        assertThat(result.getPmt()).isEqualTo(300d);
        assertThat(result.getApprovedTerms()).isEqualTo(12);
    }

    @Test
    public void shouldAcceptIfCommitmentIsLessThanPmtWith9Terms(){
        var loan = Loan.builder()
                .amount(1500d)
                .income(1900d)
                .score(900)
                .terms(6)
                .status(LoanStatus.UNDER_ANALYSIS)
                .build();
        var response = new CommitmentAdapterResponse();
        response.setCommitment(0.8);
        when(circuitBreakerFactory.create("commitment"))
                .thenReturn(circuitBreaker);
        when(circuitBreaker.run(any()))
                .thenReturn(response);
        when(pmtCalculatorService.calculatePMT(any(),eq(loan.getScore()),eq(loan.getAmount())))
                .thenReturn(560.56)
                .thenReturn(250d)
                .thenReturn(120d);

        final Loan result = policy.validate(loan);

        assertThat(result.getStatus()).isEqualTo(LoanStatus.APPROVED);
        assertThat(result.getPmt()).isEqualTo(250d);
        assertThat(result.getApprovedTerms()).isEqualTo(9);
    }
}