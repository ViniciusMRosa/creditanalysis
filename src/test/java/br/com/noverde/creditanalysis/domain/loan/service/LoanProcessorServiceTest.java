package br.com.noverde.creditanalysis.domain.loan.service;

import br.com.noverde.creditanalysis.domain.loan.Loan;
import br.com.noverde.creditanalysis.domain.loan.LoanStatus;
import br.com.noverde.creditanalysis.domain.loan.repository.LoanRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LoanProcessorServiceTest {

    @InjectMocks
    private LoanProcessorService service;
    @Mock
    private LoanAnalysisPipeline pipeline;
    @Mock
    private LoanRepository repository;
    @Captor
    private ArgumentCaptor<Loan> loanArgumentCaptor;

    @Test
    public void shouldProcess(){

        var loan = Loan.builder()
                .id(UUID.randomUUID())
                .cpf("11111111111")
                .amount(1000d)
                .terms(6)
                .name("user name")
                .birthDate(LocalDate.now().minusYears(20))
                .income(10000d)
                .status(LoanStatus.UNDER_ANALYSIS)
                .build();

        var validatedLoan = Loan.builder()
                .id(UUID.randomUUID())
                .cpf("11111111111")
                .amount(1000d)
                .terms(9)
                .score(900)
                .pmt(234d)
                .name("user name")
                .birthDate(LocalDate.now().minusYears(20))
                .income(10000d)
                .status(LoanStatus.APPROVED)
                .build();

        when(pipeline.applyCreditAnalysis(loan))
                .thenReturn(validatedLoan);

        service.processLoan(loan);

        verify(repository).save(loanArgumentCaptor.capture());

        final Loan captured = loanArgumentCaptor.getValue();
        assertThat(captured.getId()).isEqualTo(validatedLoan.getId());
        assertThat(captured.getCpf()).isEqualTo(validatedLoan.getCpf());
        assertThat(captured.getAmount()).isEqualTo(validatedLoan.getAmount());
        assertThat(captured.getTerms()).isEqualTo(validatedLoan.getTerms());
        assertThat(captured.getScore()).isEqualTo(validatedLoan.getScore());
        assertThat(captured.getPmt()).isEqualTo(validatedLoan.getPmt());
        assertThat(captured.getName()).isEqualTo(validatedLoan.getName());
        assertThat(captured.getBirthDate()).isEqualTo(validatedLoan.getBirthDate());
        assertThat(captured.getIncome()).isEqualTo(validatedLoan.getIncome());
        assertThat(captured.getStatus()).isEqualTo(validatedLoan.getStatus());
    }
}