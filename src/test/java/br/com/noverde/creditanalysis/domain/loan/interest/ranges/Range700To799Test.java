package br.com.noverde.creditanalysis.domain.loan.interest.ranges;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class Range700To799Test {
    private Range700To799 range =  new Range700To799();

    @ParameterizedTest
    @MethodSource("validValuesListProvider")
    void shouldReturnValueWhenIsValidTermAndScoreIsBetween700And799(Integer terms,Integer score, Double expected) {
        assertThat(range.calculate(terms,score).get()).isEqualTo(expected);
    }

    private static Stream<Arguments> validValuesListProvider(){
        return Stream.of(
                Arguments.of(6,700,5.5),
                Arguments.of(9,700,5.8),
                Arguments.of(12,700,6.1),
                Arguments.of(6,750,5.5),
                Arguments.of(9,750,5.8),
                Arguments.of(12,750,6.1),
                Arguments.of(6,799,5.5),
                Arguments.of(9,799,5.8),
                Arguments.of(12,799,6.1)
        );
    }



    @ParameterizedTest
    @MethodSource("invalidValuesListProvider")
    void shouldNotReturnValueWhenIsInvalidTermOrScoreIsNotBetween700And799(Integer terms,Integer score) {
        assertThat(range.calculate(terms,score)).isNotPresent();
    }

    private static Stream<Arguments> invalidValuesListProvider(){
        return Stream.of(
                Arguments.of(6,699),
                Arguments.of(9,699),
                Arguments.of(12,699),
                Arguments.of(6,800),
                Arguments.of(9,800),
                Arguments.of(12,800),
                Arguments.of(3,799),
                Arguments.of(3,799),
                Arguments.of(3,799)
        );
    }
}