package br.com.noverde.creditanalysis.domain.loan.policies;

import br.com.noverde.creditanalysis.domain.loan.Loan;
import br.com.noverde.creditanalysis.domain.loan.LoanStatus;
import br.com.noverde.creditanalysis.infrastructure.adapters.response.ScoreAdapterResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ScorePolicyTest {


    private static final String CPF = "11111111111";
    @InjectMocks
    private ScorePolicy policy;
    @Mock
    private CircuitBreakerFactory factory;
    @Mock
    private CircuitBreaker circuitBreaker;

    @Test
    public void shouldRefuseWhenScoreIsLessThan600(){
        var loan = Loan.builder()
                .cpf(CPF)
                .status(LoanStatus.UNDER_ANALYSIS)
                .build();
        var response = new ScoreAdapterResponse();
        response.setScore(500);
        when(factory.create( "score"))
                .thenReturn(circuitBreaker);
        when(circuitBreaker.run(any()))
                .thenReturn(response);

        final Loan result = policy.validate(loan);

        assertThat(result.getStatus()).isEqualTo(LoanStatus.REFUSED);
    }


    @Test
    public void shouldMaintainUnderAnalysysWhenScoreIsGreaterThan600(){
        var loan = Loan.builder()
                .cpf(CPF)
                .status(LoanStatus.UNDER_ANALYSIS)
                .build();
        var response = new ScoreAdapterResponse();
        response.setScore(601);
        when(factory.create( "score"))
                .thenReturn(circuitBreaker);
        when(circuitBreaker.run(any()))
                .thenReturn(response);

        final Loan result = policy.validate(loan);

        assertThat(result.getStatus()).isEqualTo(LoanStatus.UNDER_ANALYSIS);
    }
}