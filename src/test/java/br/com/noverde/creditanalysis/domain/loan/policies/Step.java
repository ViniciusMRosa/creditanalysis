package br.com.noverde.creditanalysis.domain.loan.policies;

public interface Step<IN,OUT> {

    OUT execute(IN input);

    default <T> Step<IN,T> execute(Step<OUT,T> source) {
        return value -> source.execute(execute(value));
    }
}
