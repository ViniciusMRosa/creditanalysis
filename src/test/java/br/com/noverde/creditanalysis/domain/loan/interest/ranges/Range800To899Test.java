package br.com.noverde.creditanalysis.domain.loan.interest.ranges;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class Range800To899Test {
    private Range800To899 range =  new Range800To899();

    @ParameterizedTest
    @MethodSource("validValuesListProvider")
    void shouldReturnValueWhenIsValidTermAndScoreIsBetween800And899(Integer terms,Integer score, Double expected) {
        assertThat(range.calculate(terms,score).get()).isEqualTo(expected);
    }

    private static Stream<Arguments> validValuesListProvider(){
        return Stream.of(
                Arguments.of(6,800,4.7),
                Arguments.of(9,800,5.0),
                Arguments.of(12,800,5.3),
                Arguments.of(6,850,4.7),
                Arguments.of(9,850,5.0),
                Arguments.of(12,850,5.3),
                Arguments.of(6,899,4.7),
                Arguments.of(9,899,5.0),
                Arguments.of(12,899,5.3)
        );
    }



    @ParameterizedTest
    @MethodSource("invalidValuesListProvider")
    void shouldNotReturnValueWhenIsInvalidTermOrScoreIsNotBetween800And899(Integer terms,Integer score) {
        assertThat(range.calculate(terms,score)).isNotPresent();
    }

    private static Stream<Arguments> invalidValuesListProvider(){
        return Stream.of(
                Arguments.of(6,699),
                Arguments.of(9,699),
                Arguments.of(12,699),
                Arguments.of(6,700),
                Arguments.of(9,700),
                Arguments.of(12,700),
                Arguments.of(3,799),
                Arguments.of(3,799),
                Arguments.of(3,799)
        );
    }
}