package br.com.noverde.creditanalysis.application.loan;

import br.com.noverde.creditanalysis.CreditanalysisApplication;
import br.com.noverde.creditanalysis.application.error.Error;
import br.com.noverde.creditanalysis.application.error.ErrorResponse;
import br.com.noverde.creditanalysis.domain.loan.Loan;
import br.com.noverde.creditanalysis.domain.loan.repository.LoanRepository;
import br.com.noverde.creditanalysis.domain.loan.service.LoanProcessorService;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = CreditanalysisApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class LoanControllerTest {

    @LocalServerPort
    private int port;
    private RequestSpecification request;
    @MockBean
    private LoanProcessorService processorService;
    @Autowired
    private LoanRepository repository;

    @BeforeEach
    public void setup(){
        RestAssured.port = port;
        request = RestAssured.given().with().contentType(MediaType.APPLICATION_JSON_VALUE);

    }

    @Test
    public void shouldReturn400WhenFieldsAreNotFilled() throws Exception {
        var createRequest = new LoanCreateRequest();

        final ErrorResponse response = request.body(createRequest)
                .when()
                .post("/loan")
                .then()
                .statusCode(400)
                .and()
                .extract().body().as(ErrorResponse.class);

        Stream.of("name","birthDate","terms","cpf")
                .forEach( code ->{
                    final List<Error> error = response.getErrors().stream().filter(err -> err.getCode().equalsIgnoreCase(code))
                            .collect(Collectors.toList());
                    assertThat(error).hasSize(1);
                    assertThat(error.get(0).getMessage()).isNotNull();

                });
    }


    @Test
    public void shouldReturn400WhenTermsAreDiferentFromAllowedValues() throws Exception {
        var createRequest = createValidRequestDTO();
        createRequest.setTerms(8);

        final ErrorResponse response = request.body(createRequest)
                .when()
                .post("/loan")
                .then()
                .statusCode(400)
                .and()
                .extract().body().as(ErrorResponse.class);

        assertThat(response.getErrors()).hasSize(1);
        assertThat(response.getErrors().get(0).getMessage()).isEqualTo("O número de parcelas só pode ser 6, 9 ou 12");
    }


    @Test
    public void shouldReturn400WhenCPFIsInvalid() throws Exception {
        var createRequest = createValidRequestDTO();
        createRequest.setCpf("11111111112");

        final ErrorResponse response = request.body(createRequest)
                .when()
                .post("/loan")
                .then()
                .statusCode(400)
                .and()
                .extract().body().as(ErrorResponse.class);

        assertThat(response.getErrors()).hasSize(1);
        assertThat(response.getErrors().get(0).getMessage()).isEqualTo("CPF Inválido");
    }

    @Test
    public void shouldReturn400WhenBirthDateIsInTheFuture() throws Exception {
        var createRequest = createValidRequestDTO();
        createRequest.setBirthDate(LocalDate.now());

        final ErrorResponse response = request.body(createRequest)
                .when()
                .post("/loan")
                .then()
                .statusCode(400)
                .and()
                .extract().body().as(ErrorResponse.class);

        assertThat(response.getErrors()).hasSize(1);
        assertThat(response.getErrors().get(0).getMessage()).isEqualTo("Data de nascimento deve ser uma data passada");
    }

    @Test
    public void shouldReturn400WhenAmountIsLessThan1000() throws Exception {
        var createRequest = createValidRequestDTO();
        createRequest.setAmount(900d);

        final ErrorResponse response = request.body(createRequest)
                .when()
                .post("/loan")
                .then()
                .statusCode(400)
                .and()
                .extract().body().as(ErrorResponse.class);

        assertThat(response.getErrors()).hasSize(1);
        assertThat(response.getErrors().get(0).getMessage()).isEqualTo("O valor para empréstimo tem um mínimo de R$ 1.000,00");
    }

    @Test
    public void shouldReturn400WhenAmountIsMoreThan4000() throws Exception {
        var createRequest = createValidRequestDTO();
        createRequest.setAmount(9000d);

        final ErrorResponse response = request.body(createRequest)
                .when()
                .post("/loan")
                .then()
                .statusCode(400)
                .and()
                .extract().body().as(ErrorResponse.class);

        assertThat(response.getErrors()).hasSize(1);
        assertThat(response.getErrors().get(0).getMessage()).isEqualTo("O valor para empréstimo tem um máximo de R$ 4.000,00");

    }

    @Test
    public void shouldReturn200AndIDWhenIsValidLoan() throws Exception {
        var createRequest = createValidRequestDTO();
        doNothing()
            .when(processorService).processLoan(any());

        final LoanCreateResponse response = request.body(createRequest)
                .when()
                .post("/loan")
                .then()
                .statusCode(200)
                .and()
                .extract().body().as(LoanCreateResponse.class);

        assertThat(response.getId()).isNotNull();
    }

    @Test
    public void shouldReturnLoanStatus() throws Exception {
        var loan = createValidRequestDTO().buildLoan();
        loan.setupPending();
        final Loan saved = repository.save(loan);

        final LoanStatusResponse response = request
                .when()
                .get("/loan/" + saved.getId())
                .then()
                .statusCode(200)
                .and()
                .extract().body().as(LoanStatusResponse.class);

        assertThat(response.getId()).isNotNull();
        assertThat(response.getStatus()).isEqualTo("processing");
        assertThat(response.getResult()).isNull();
        assertThat(response.getRefusedPolicy()).isNull();
        assertThat(response.getAmount()).isNull();
        assertThat(response.getTerms()).isNull();
    }

    private LoanCreateRequest createValidRequestDTO(){
        var request = new LoanCreateRequest();

        request.setName("User name");
        request.setCpf("55611468071");
        request.setBirthDate(LocalDate.now().minusYears(20));
        request.setAmount(1000d);
        request.setTerms(9);
        request.setIncome(1000d);
        return request;
    }
}