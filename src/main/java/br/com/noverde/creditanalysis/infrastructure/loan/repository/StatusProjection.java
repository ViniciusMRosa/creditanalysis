package br.com.noverde.creditanalysis.infrastructure.loan.repository;

import br.com.noverde.creditanalysis.domain.loan.LoanStatus;

public interface StatusProjection {
    LoanStatus getStatus();
}
