package br.com.noverde.creditanalysis.infrastructure.adapters.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScoreAdapterResponse {
    private Integer score;
}
