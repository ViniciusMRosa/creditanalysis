package br.com.noverde.creditanalysis.infrastructure.adapters.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(staticName = "of")
@Getter
public class ApiRequestDTO {
    private String cpf;
}
