package br.com.noverde.creditanalysis.infrastructure.configuration;

import feign.RequestInterceptor;
import feign.Retryer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AdapterConfiguration {

    @Value("${noverde.api.key}")
    private String apiKey;
    private static final String X_API_KEY_HEADER_NAME = "x-api-key";

    @Bean
    public RequestInterceptor getRequestINterceptor(){
        return (template) -> template.header(X_API_KEY_HEADER_NAME,apiKey);
    }

    @Bean
    public Retryer getRetryer(){
        return new Retryer.Default(1000l,2l,5);
    }
}
