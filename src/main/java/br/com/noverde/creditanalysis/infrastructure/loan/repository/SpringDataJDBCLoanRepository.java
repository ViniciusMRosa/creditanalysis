package br.com.noverde.creditanalysis.infrastructure.loan.repository;

import br.com.noverde.creditanalysis.domain.loan.Loan;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

public interface SpringDataJDBCLoanRepository extends CrudRepository<Loan, UUID> {

    @Query("select status from loan where id = :id")
    Optional<StatusProjection>  findStatusById(@Param("id") UUID id);
}
