package br.com.noverde.creditanalysis.infrastructure.loan.repository;

import br.com.noverde.creditanalysis.domain.loan.Loan;
import br.com.noverde.creditanalysis.domain.loan.repository.LoanRepository;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
@Primary
@AllArgsConstructor
public class RelationalLoanRepository implements LoanRepository {

    private SpringDataJDBCLoanRepository repository;

    @Override
    public Optional<Loan> getById(UUID id) {
        return repository.findById(id);
    }

    @Override
    public Loan save(Loan loan) {
        return repository.save(loan);
    }
}
