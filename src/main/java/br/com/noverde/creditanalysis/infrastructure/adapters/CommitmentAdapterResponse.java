package br.com.noverde.creditanalysis.infrastructure.adapters;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommitmentAdapterResponse {
    private Double commitment;
}
