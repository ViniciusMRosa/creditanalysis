package br.com.noverde.creditanalysis.infrastructure.adapters;

import br.com.noverde.creditanalysis.infrastructure.adapters.request.ApiRequestDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Map;

@FeignClient(url = "${noverde.api.base.url}", name = "commitment-client")
public interface CommitmentAdapter {

    @PostMapping("/commitment")
    CommitmentAdapterResponse getCommitment(ApiRequestDTO params);
}
