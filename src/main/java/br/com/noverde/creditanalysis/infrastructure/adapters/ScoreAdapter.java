package br.com.noverde.creditanalysis.infrastructure.adapters;

import br.com.noverde.creditanalysis.infrastructure.adapters.request.ApiRequestDTO;
import br.com.noverde.creditanalysis.infrastructure.adapters.response.ScoreAdapterResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Map;

@FeignClient(url = "${noverde.api.base.url}", name = "score-client")
public interface ScoreAdapter {

    @PostMapping("/score")
    ScoreAdapterResponse getScore(ApiRequestDTO params);
}
