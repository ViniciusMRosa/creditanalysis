package br.com.noverde.creditanalysis.application.rest;

import br.com.noverde.creditanalysis.application.error.Error;
import br.com.noverde.creditanalysis.application.error.ErrorResponse;
import br.com.noverde.creditanalysis.domain.loan.exceptions.LoanNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;
import java.util.stream.Collectors;


@ControllerAdvice
public class LoanExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleInputValidationExceptions(MethodArgumentNotValidException ex){

       var errors = ex.getBindingResult().getFieldErrors().stream()
                .map(fe -> Error.of(fe.getField(),fe.getDefaultMessage()))
                .collect(Collectors.toList());
        return ErrorResponse.of(errors);
    }

    @ExceptionHandler(LoanNotFoundException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse handleInputValidationExceptions(LoanNotFoundException ex){

       var errors = List.of(Error.of("404","Empréstimo não encontrado"));
        return ErrorResponse.of(errors);
    }


}
