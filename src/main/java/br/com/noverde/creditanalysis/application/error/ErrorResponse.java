package br.com.noverde.creditanalysis.application.error;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@Getter
public class ErrorResponse {

    private List<Error> errors;
}
