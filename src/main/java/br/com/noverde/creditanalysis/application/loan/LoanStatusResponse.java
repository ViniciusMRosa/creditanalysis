package br.com.noverde.creditanalysis.application.loan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Builder
public class LoanStatusResponse {
    private UUID id;
    private String status;
    private String result;
    @JsonProperty("refused_policy")
    private String refusedPolicy;
    private Double amount;
    private Integer terms;
}
