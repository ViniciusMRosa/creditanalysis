package br.com.noverde.creditanalysis.application.loan;

import br.com.noverde.creditanalysis.domain.loan.Loan;
import br.com.noverde.creditanalysis.domain.loan.validations.TermsValidation;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.*;
import java.time.LocalDate;


@Getter
@Setter
@ToString
public class LoanCreateRequest {
    @NotBlank(message = "Nome deve ser preenchido")
    private String name;
    @NotBlank(message = "CPF Deve ser preenchido")
    @CPF(message = "CPF Inválido")
    private String cpf;
    @NotNull(message = "Data de nascimento deve ser informada")
    @Past(message = "Data de nascimento deve ser uma data passada")
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate birthDate;
    @Min(value = 1000,message = "O valor para empréstimo tem um mínimo de R$ 1.000,00")
    @Max(value = 4000,message = "O valor para empréstimo tem um máximo de R$ 4.000,00")
    private Double amount;
    //@Pattern(regexp = "^(6|9|12)$", message = "Só é possível 6, 9 ou 12 parcelas")
    @TermsValidation(message = "O número de parcelas só pode ser 6, 9 ou 12")
    private Integer terms;
    @Positive(message = "Renda mensal deve ser um valor positivo")
    @Digits(integer = Integer.MAX_VALUE,fraction = 2,message = "Renda mensal informada incorretamente")
    private Double income;


    public Loan buildLoan(){
        return Loan.builder()
                .name(this.getName())
                .cpf(this.getCpf())
                .birthDate(this.getBirthDate())
                .amount(this.getAmount())
                .terms(this.getTerms())
                .income(this.getIncome())
                .build();
    }
}
