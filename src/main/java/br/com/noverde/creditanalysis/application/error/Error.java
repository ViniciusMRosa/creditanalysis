package br.com.noverde.creditanalysis.application.error;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(staticName = "of")
@Getter
public class Error {

    private String code;
    private String message;
}
