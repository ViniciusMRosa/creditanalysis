package br.com.noverde.creditanalysis.application.loan;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Getter
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
public class LoanCreateResponse {
    private UUID id;
}
