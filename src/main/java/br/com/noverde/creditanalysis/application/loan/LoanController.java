package br.com.noverde.creditanalysis.application.loan;

import br.com.noverde.creditanalysis.domain.loan.service.LoanCreateService;
import br.com.noverde.creditanalysis.domain.loan.service.LoanFinderService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/loan")
@AllArgsConstructor
@Slf4j
public class LoanController {

    private LoanCreateService createService;
    private LoanFinderService finderService;

    @PostMapping
    public LoanCreateResponse createLoanRequest(@Validated @RequestBody  LoanCreateRequest request){
        log.info("Loan create request received {}", request);
        final UUID loanId = createService.createLoanRequest(request.buildLoan());
        return LoanCreateResponse.of(loanId);
    }

    @GetMapping("/{id}")
    public LoanStatusResponse getLoanStatus(@PathVariable("id") UUID id){
        return finderService.getLoanById(id);
    }
}
