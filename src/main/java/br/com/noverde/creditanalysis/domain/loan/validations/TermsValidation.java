package br.com.noverde.creditanalysis.domain.loan.validations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = ValidTermsValidator.class)
public @interface TermsValidation {

    String message() default "br.com.noverde.terms.invalid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
