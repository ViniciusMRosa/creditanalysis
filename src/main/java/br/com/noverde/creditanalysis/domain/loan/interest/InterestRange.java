package br.com.noverde.creditanalysis.domain.loan.interest;

import java.util.Optional;

public interface InterestRange {

    Optional<Double> calculate(Integer terms, Integer score);

}
