package br.com.noverde.creditanalysis.domain.loan.service;

import br.com.noverde.creditanalysis.domain.loan.Loan;

import java.util.UUID;

public interface LoanCreateService {

    UUID createLoanRequest(Loan loan);

}
