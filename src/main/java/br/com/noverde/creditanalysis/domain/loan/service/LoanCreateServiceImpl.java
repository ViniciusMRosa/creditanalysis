package br.com.noverde.creditanalysis.domain.loan.service;

import br.com.noverde.creditanalysis.domain.loan.Loan;
import br.com.noverde.creditanalysis.domain.loan.LoanStatus;
import br.com.noverde.creditanalysis.domain.loan.exceptions.LoanNotFoundException;
import br.com.noverde.creditanalysis.domain.loan.repository.LoanRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Primary
@AllArgsConstructor
@Slf4j
public class LoanCreateServiceImpl implements LoanCreateService{

    private LoanRepository repository;
    private LoanProcessorService processorService;

    @Override
    public UUID createLoanRequest(Loan loan) {
        loan.setupPending();
        var saved = repository.save(loan);
        log.info("Loan saved {}", saved);
        processorService.processLoan(saved);
        return saved.getId();
    }
}
