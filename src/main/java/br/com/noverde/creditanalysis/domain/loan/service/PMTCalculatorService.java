package br.com.noverde.creditanalysis.domain.loan.service;

import br.com.noverde.creditanalysis.domain.loan.interest.InterestCalculatorChain;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class PMTCalculatorService {

    private InterestCalculatorChain interestCalculatorChain;


    public Double calculatePMT(Integer terms, Integer score, Double amount){
        return amount * calculateFraction(terms,score);
    }

    private Double calculateFraction(Integer terms,Integer score) {
        var interest = interestCalculatorChain.calculateInterest(terms,score);
        return calculateNumerator(interest,terms)/calculateDenominator(interest,terms);
    }

    private Double calculateDenominator(Double interest, Integer terms) {
        return Math.pow(1.0 + interest,terms) - 1;

    }

    private Double calculateNumerator(Double interest, Integer terms) {
        return Math.pow(1.0 + interest,terms) * interest ;
    }

}
