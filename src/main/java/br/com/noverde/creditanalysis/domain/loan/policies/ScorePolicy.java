package br.com.noverde.creditanalysis.domain.loan.policies;

import br.com.noverde.creditanalysis.domain.loan.Loan;
import br.com.noverde.creditanalysis.domain.loan.LoanStatus;
import br.com.noverde.creditanalysis.infrastructure.adapters.ScoreAdapter;
import br.com.noverde.creditanalysis.infrastructure.adapters.request.ApiRequestDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@Slf4j
public class ScorePolicy implements Policy<Loan,Loan> {
    private static final String POLICY_NAME = "score";
    private static final String API_TOKEN = "score.api-token";
    private CircuitBreakerFactory circuitBreakerFactory;
    private ScoreAdapter adapter;

    @Override
    public Loan validate(Loan loan) {
        if(loan.getStatus() != LoanStatus.UNDER_ANALYSIS)
            return loan;
        log.info("Applying score policy to loan {}",loan.getId());
        var result = circuitBreakerFactory
                .create(POLICY_NAME)
                .run(() -> adapter.getScore(ApiRequestDTO.of(loan.getCpf())));
        loan.setScore(result.getScore());
        if(result.getScore() < 600d){
            return loan.refuse(POLICY_NAME);
        }
        return loan.analysing();
    }
}