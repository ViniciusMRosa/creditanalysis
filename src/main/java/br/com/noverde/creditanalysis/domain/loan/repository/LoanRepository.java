package br.com.noverde.creditanalysis.domain.loan.repository;

import br.com.noverde.creditanalysis.domain.loan.Loan;

import java.util.Optional;
import java.util.UUID;

public interface LoanRepository {

    Optional<Loan> getById(UUID id);

    Loan save(Loan loan);

}
