package br.com.noverde.creditanalysis.domain.loan.policies;

import br.com.noverde.creditanalysis.domain.loan.Loan;
import br.com.noverde.creditanalysis.domain.loan.LoanStatus;
import br.com.noverde.creditanalysis.domain.loan.service.PMTCalculatorService;
import br.com.noverde.creditanalysis.infrastructure.adapters.CommitmentAdapter;
import br.com.noverde.creditanalysis.infrastructure.adapters.request.ApiRequestDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@Slf4j
public class CommitmentPolicy implements Policy<Loan,Loan>{
    private static final String POLICY_NAME = "commitment";
    private static final String API_KEY = "commitment.api.key";
    private CommitmentAdapter adapter;
    private CircuitBreakerFactory circuitBreakerFactory;
    private Environment environment;
    private PMTCalculatorService pmtCalculatorService;

    public Loan validate(Loan loan){
        if(loan.getStatus() != LoanStatus.UNDER_ANALYSIS)
            return loan;

        log.info("Applying commitment policy to loan {}",loan.getId());
        var result = circuitBreakerFactory.create(POLICY_NAME)
                .run(() ->adapter.getCommitment(ApiRequestDTO.of(loan.getCpf())));

        var availableIncome = loan.getIncome() - (loan.getIncome() * result.getCommitment());

        for(int term = loan.getTerms(); term<= 12;term+=3){
            final Double pmt = pmtCalculatorService.calculatePMT(term, loan.getScore(), loan.getAmount());
            log.info("trying to accept loan {} with {} terms and commitment {} and pmt {}",loan.getId()
                    ,term,result.getCommitment(),pmt);

            if(availableIncome >= pmt){
                log.info("Accepted with {} terms and pmt {}",term,pmt);
                return loan.accept(term,pmt);
            }
        }

        log.info("Refusing loan {} by commitment problems",loan.getId());
        return loan.refuse(POLICY_NAME);
    }

}
