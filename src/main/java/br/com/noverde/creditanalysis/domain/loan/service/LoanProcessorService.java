package br.com.noverde.creditanalysis.domain.loan.service;

import br.com.noverde.creditanalysis.domain.loan.Loan;
import br.com.noverde.creditanalysis.domain.loan.LoanStatus;
import br.com.noverde.creditanalysis.domain.loan.repository.LoanRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class LoanProcessorService {

    private LoanAnalysisPipeline pipeline;
    private LoanRepository repository;

    @Async
    public void processLoan(Loan loan){
        try {
            log.info("Started to validate credito conditions to loan {}", loan.getId());
            final Loan validatedLoan = pipeline.applyCreditAnalysis(loan);
            log.info("Loan {} validated with status {}", loan.getId(),loan.getStatus());
            repository.save(validatedLoan);
        } catch (Exception e) {
           loan.setStatus(LoanStatus.ERROR);
           log.error("Error while trying to validate loan {}", loan.getId(),e);
           repository.save(loan);
        }
    }
}
