package br.com.noverde.creditanalysis.domain.loan.validations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;
import java.util.Objects;

public class ValidTermsValidator implements ConstraintValidator<TermsValidation, Integer> {

   private static final List<Integer> VALID_VALUES = List.of(6,9,12);
   public void initialize(TermsValidation constraint) {
   }

   public boolean isValid(Integer term, ConstraintValidatorContext context) {
      return !Objects.isNull(term) && VALID_VALUES.contains(term);
   }
}
