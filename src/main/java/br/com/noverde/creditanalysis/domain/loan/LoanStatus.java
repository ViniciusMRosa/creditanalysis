package br.com.noverde.creditanalysis.domain.loan;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum LoanStatus {
    REQUESTED("processing"),
    APPROVED("completed"),
    REFUSED("completed"),
    UNDER_ANALYSIS("processing"),
    ERROR("completed");
    private final String description;
}

