package br.com.noverde.creditanalysis.domain.loan.service;

import br.com.noverde.creditanalysis.domain.loan.Loan;
import br.com.noverde.creditanalysis.domain.loan.policies.AgePolicy;
import br.com.noverde.creditanalysis.domain.loan.policies.CommitmentPolicy;
import br.com.noverde.creditanalysis.domain.loan.policies.Policy;
import br.com.noverde.creditanalysis.domain.loan.policies.ScorePolicy;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class LoanAnalysisPipeline {

    private Policy<Loan,Loan> policiesPipeline;
    @NonNull
    private AgePolicy agePolicy;
    @NonNull
    private ScorePolicy scorePolicy;
    @NonNull
    private CommitmentPolicy commitmentPolicy;

    @PostConstruct
    public void initPipeline(){
        var scoreStep = agePolicy.pipe( as -> scorePolicy.validate(as));
        policiesPipeline = scoreStep.pipe(sp ->commitmentPolicy.validate(sp));
    }

    public Loan applyCreditAnalysis(Loan loan){
        return policiesPipeline.validate(loan);
    }
}
