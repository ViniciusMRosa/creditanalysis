package br.com.noverde.creditanalysis.domain.loan.interest.ranges;

import br.com.noverde.creditanalysis.domain.loan.interest.InterestRange;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;

@Component
public class Range600To699 implements InterestRange, Ordered {

    private final Map<Integer,Double> interestsByTerm = Map.of(6,6.4,9,6.6,12,6.9);

    @Override
    public Optional<Double> calculate(Integer terms, Integer score) {
        if(score>= 600 && score < 700 )
            return Optional.ofNullable(interestsByTerm.get(terms));
        return Optional.empty();
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }
}
