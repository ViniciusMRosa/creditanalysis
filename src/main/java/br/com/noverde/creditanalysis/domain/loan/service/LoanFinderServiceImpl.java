package br.com.noverde.creditanalysis.domain.loan.service;

import br.com.noverde.creditanalysis.application.loan.LoanStatusResponse;
import br.com.noverde.creditanalysis.domain.loan.Loan;
import br.com.noverde.creditanalysis.domain.loan.LoanStatus;
import br.com.noverde.creditanalysis.domain.loan.exceptions.LoanNotFoundException;
import br.com.noverde.creditanalysis.domain.loan.repository.LoanRepository;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Primary
@AllArgsConstructor
public class LoanFinderServiceImpl implements LoanFinderService {

    private LoanRepository repository;

    @Override
    public LoanStatusResponse getLoanById(UUID id) {
        final Loan loan = repository.getById(id)
                .orElseThrow(LoanNotFoundException::new);

        return LoanStatusResponse.builder()
                .id(loan.getId())
                .status(loan.getStatus().getDescription())
                .result(getResult(loan.getStatus()))
                .refusedPolicy(loan.getRefusedPolicy())
                .amount(LoanStatus.APPROVED == loan.getStatus() ? loan.getAmount() : null)
                .terms(loan.getApprovedTerms())
                .build();
    }

    private String getResult(LoanStatus status) {

        switch (status){
            case APPROVED:
            case REFUSED:
                return status.toString().toLowerCase();
            default:
                return null;
        }
    }
}
