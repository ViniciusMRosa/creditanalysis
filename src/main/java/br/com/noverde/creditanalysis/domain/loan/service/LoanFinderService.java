package br.com.noverde.creditanalysis.domain.loan.service;

import br.com.noverde.creditanalysis.application.loan.LoanStatusResponse;
import br.com.noverde.creditanalysis.domain.loan.Loan;

import java.util.UUID;

public interface LoanFinderService {

    LoanStatusResponse getLoanById(UUID id);
}
