package br.com.noverde.creditanalysis.domain.loan.interest;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;

@Component
@AllArgsConstructor
@Slf4j
public class InterestCalculatorChain {

    private List<InterestRange> interestRanges;


    @PostConstruct
    public void setup(){
        Collections.sort(interestRanges, AnnotationAwareOrderComparator.INSTANCE);
    }

    public Double calculateInterest(Integer terms,Integer score){
        log.info("Calculating interest with terms:{} and score {}",terms,score);
        for(InterestRange range: interestRanges){
            var result = range.calculate(terms,score);
            if(result.isPresent()){
                final double interest = result.get() / 100d;
                log.info("returning interest {}",interest);
                return interest;
            }
        }

        return 0d;
    }
}
