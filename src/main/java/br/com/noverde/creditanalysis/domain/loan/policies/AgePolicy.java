package br.com.noverde.creditanalysis.domain.loan.policies;

import br.com.noverde.creditanalysis.domain.loan.Loan;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.Period;

@Component
@Slf4j
public class AgePolicy implements Policy<Loan,Loan> {
    private static final String POLICY_NAME = "age";
    @Override
    public Loan validate(Loan loan) {
        log.info("Applying age policy to loan {}", loan.getId());
        if(Period.between(loan.getBirthDate(), LocalDate.now()).getYears() < 18)
           return loan.refuse(POLICY_NAME);
        return loan.analysing();
    }
}
