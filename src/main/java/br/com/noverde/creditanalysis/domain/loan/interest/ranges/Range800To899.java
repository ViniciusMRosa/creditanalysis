package br.com.noverde.creditanalysis.domain.loan.interest.ranges;

import br.com.noverde.creditanalysis.domain.loan.interest.InterestRange;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;

@Component
public class Range800To899 implements InterestRange, Ordered {

    private final Map<Integer,Double> interestsByTerm = Map.of(6,4.7,9,5.0,12,5.3);

    @Override
    public Optional<Double> calculate(Integer terms, Integer score) {
        if(score>= 800 && score < 900)
            return Optional.ofNullable(interestsByTerm.get(terms));
        return Optional.empty();    }

    @Override
    public int getOrder() {
        return 2;
    }
}
