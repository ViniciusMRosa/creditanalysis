package br.com.noverde.creditanalysis.domain.loan.policies;

import br.com.noverde.creditanalysis.domain.loan.Loan;

public interface Policy<IN,OUT> {

    OUT validate(IN value);

    default <R> Policy<IN, R> pipe(Policy<OUT, R> source) {
        return value -> source.validate(validate(value));
    }

}
