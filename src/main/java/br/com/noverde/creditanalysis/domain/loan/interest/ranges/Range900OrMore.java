package br.com.noverde.creditanalysis.domain.loan.interest.ranges;

import br.com.noverde.creditanalysis.domain.loan.interest.InterestRange;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;

@Component
public class Range900OrMore implements InterestRange, Ordered {

    private final Map<Integer,Double> interestsByTerm = Map.of(6,3.9,9,4.2,12,4.5);

    @Override
    public int getOrder() {
        return LOWEST_PRECEDENCE;
    }

    @Override
    public Optional<Double> calculate(Integer terms, Integer score) {
        if(score>= 900)
            return Optional.ofNullable(interestsByTerm.get(terms));
        return Optional.empty();
    }
}
