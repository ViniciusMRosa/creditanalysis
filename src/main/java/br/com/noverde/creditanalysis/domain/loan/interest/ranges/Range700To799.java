package br.com.noverde.creditanalysis.domain.loan.interest.ranges;

import br.com.noverde.creditanalysis.domain.loan.interest.InterestRange;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;

@Component
public class Range700To799 implements InterestRange, Ordered {

    private final Map<Integer,Double> interestsByTerm = Map.of(6,5.5,9,5.8,12,6.1);

    @Override
    public Optional<Double> calculate(Integer terms, Integer score) {
        if(score>= 700 && score < 800)
            return Optional.ofNullable(interestsByTerm.get(terms));
        return Optional.empty();
    }

    @Override
    public int getOrder() {
        return 1;
    }
}
