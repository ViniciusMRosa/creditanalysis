package br.com.noverde.creditanalysis.domain.loan;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDate;
import java.util.UUID;

@Builder
@Data
public class Loan {
    @Id
    private UUID id;
    private String name;
    private String cpf;
    private LocalDate birthDate;
    private Double amount;
    private Integer terms;
    private Double income;
    private LoanStatus status;
    private Integer score;
    private Double pmt;
    private String refusedPolicy;
    private Integer approvedTerms;

    public void setupPending(){
        this.status = LoanStatus.UNDER_ANALYSIS;
    }

    public Loan refuse(String refusedPolicy){
        this.status = LoanStatus.REFUSED;
        this.refusedPolicy = refusedPolicy;
        return this;
    }

    public Loan analysing(){
        this.status = LoanStatus.UNDER_ANALYSIS;
        return this;
    }

    public Loan accept(Integer terms,Double pmt){
        this.status = LoanStatus.APPROVED;
        this.approvedTerms =terms;
        this.pmt = pmt;
        return this;
    }
}
