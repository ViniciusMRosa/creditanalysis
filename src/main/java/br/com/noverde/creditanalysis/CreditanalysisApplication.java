package br.com.noverde.creditanalysis;

import br.com.noverde.creditanalysis.domain.loan.Loan;
import br.com.noverde.creditanalysis.infrastructure.loan.repository.SpringDataJDBCLoanRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;
import org.springframework.data.jdbc.repository.config.AbstractJdbcConfiguration;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;
import org.springframework.data.jdbc.repository.config.JdbcConfiguration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableJdbcRepositories(basePackageClasses = SpringDataJDBCLoanRepository.class)
@EntityScan(basePackageClasses = Loan.class)
@Import(AbstractJdbcConfiguration.class)
@EnableWebMvc
@EnableAsync
@EnableFeignClients
public class CreditanalysisApplication {

    public static void main(String[] args) {
        SpringApplication.run(CreditanalysisApplication.class, args);
    }

}
