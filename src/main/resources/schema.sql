CREATE TABLE IF NOT EXISTS loan
(id UUID DEFAULT random_uuid() PRIMARY KEY ,
name VARCHAR(100),
cpf CHAR(11),
birth_date date,
amount decimal(10,2),
terms tinyint,
income decimal(10,2),
status varchar(30),
score int,
pmt decimal(10,2),
approved_terms tinyint,
refused_policy varchar(20)
);