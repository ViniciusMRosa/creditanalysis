## Noverde - Análise de crédito

Avaliação de crédito utilizando as APIs de score e commitment.

## Tecnologias utilizadas
 - Java 11
 - Spring Boot
 - Spring Data JDBC
 - H2 Database
 - Spring Cloud Circuit Breaker
 - Spring Cloud Open Feign
 - Spring Cloud Netflix Hystrix

### Pre requisitos
    
    - java 11+
    - maven 3.6+(Existe um wrapper dentro do projeto)

##Como  rodar

Há duas maneiras possíveis para exxecutarmos o projeto.
A primeira e mais simples é utilizar o utilitário `make`, que é bastante comum em ambientes baseados em Unix.

Para utilizar dessa forma basta executar o comando a seguir
```
make run 
```

Caso não possua ou não queira utilizar o `make` são necessários dois passos, o primeiro faz o build do projeto e em seguida podemos executar o jar gerado

Passos:
```
./mvnw clean install
java -jar target/creditanalysis-0.0.1-SNAPSHOT.jar
```
   
## Melhorias futuras

 - Criação de um método para tentar novamente,
  de acordo com uma quantidade configurada, analisar os empréstimos que tiveram erro durante a tentativa de análise. Apesar de ter um mecanismo de retry nas chamadas dos endpoints, algumas vezes vi que ocorria timeout na chamada em todas as vezes que o retruy era feito
 - Dockerizar a aplicação
    

